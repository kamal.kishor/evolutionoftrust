import java.util.Scanner;

public class ConsoleBehaviour implements PlayerBehaviour {
    private Scanner scanner;

    public ConsoleBehaviour(Scanner scanner) {
        this.scanner = scanner;
    }

    @Override
    public String makeMove() {
        String input = scanner.next();
        if ("1".equalsIgnoreCase(input)) {
            return "1";
        }
        return "0";
    }
}