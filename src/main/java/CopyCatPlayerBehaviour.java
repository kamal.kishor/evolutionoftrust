import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

public class CopyCatPlayerBehaviour implements  PlayerBehaviour, Observer {

    private String opponentPlayerMove=null;
    private String ownMove=null;

    @Override
    public String makeMove() {

        if(opponentPlayerMove == null) {
            this.ownMove=MoveType.COOPERATE.value;
            return MoveType.COOPERATE.value;
        }
        else {
            this.ownMove = opponentPlayerMove;
            return opponentPlayerMove;
        }

    }

    @Override
    public void update(Observable observable, Object playerMoves)
    {
        Map moveType=(HashMap)playerMoves;
        if(moveType.get("Player1Move").toString().equals(this.ownMove))
            this.opponentPlayerMove = moveType.get("Player2Move").toString();
        else
            this.opponentPlayerMove = moveType.get("Player1Move").toString();
    }
}
