import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

public class Game extends Observable {
    private Player player1;
    private Player player2;
    private Machine machine;
    private PrintStream printStream;

    public Game(Player player1, Player player2, Machine machine, PrintStream printStream) {
       this.player1 = player1;
       this.player2 = player2;
       this.machine = machine;
       this.printStream = printStream;
    }


    public void playRound(int numOfRounds) {
        while(numOfRounds-- > 0) {
            String moveForPlayer1 = player1.makeMove();
            String moveForPlayer2 = player2.makeMove();

            Map playerMoves=new HashMap();
            playerMoves.put("Player1Move",moveForPlayer1);
            playerMoves.put("Player2Move",moveForPlayer2);



            Map<String, Integer> score = machine.getPlayerScores(moveForPlayer1, moveForPlayer2);
            player1.setScore(player1.getScore() + score.get("P1Score"));
            player2.setScore(player2.getScore() +score.get("P2Score"));

            setChanged();
            notifyObservers(playerMoves);
            printStream.println("P1Score: " + player1.getScore() + ", P2Score: " + player2.getScore());
        }

    }
}
