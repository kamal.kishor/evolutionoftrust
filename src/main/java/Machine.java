import com.sun.tools.javac.code.Attribute;

import java.util.HashMap;
import java.util.Map;

public class Machine {


    public Map<String, Integer> getPlayerScores(String player1Move, String player2Move) {

        Map<String, Integer> scores = new HashMap<>();
        if(player1Move==MoveType.COOPERATE.value && player2Move==MoveType.COOPERATE.value){
            scores.put("P1Score", 2);
            scores.put("P2Score", 2);
        }else if(player1Move==MoveType.CHEAT.value && player2Move==MoveType.CHEAT.value){
            scores.put("P1Score", 0);
            scores.put("P2Score", 0);
        }else if(player1Move==MoveType.COOPERATE.value && player2Move==MoveType.CHEAT.value){
            scores.put("P1Score", -1);
            scores.put("P2Score", 3);
        }else if(player1Move==MoveType.CHEAT.value && player2Move==MoveType.COOPERATE.value){
            scores.put("P1Score",3);
            scores.put("P2Score",-1);
        }
        return scores;



    }
}
