public class Player {
    private final PlayerBehaviour playerBehaviour;
    private int score;

    public Player(PlayerBehaviour playerBehaviour) {
        this.playerBehaviour = playerBehaviour;
        this.score=0;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }


    public String makeMove() {
        return playerBehaviour.makeMove();
    }
}
