import org.junit.Assert;
import org.junit.Test;

public class AlwaysCheatBehaviourTest {
    AlwaysCheatBehaviour alwaysCheatBehaviour = new AlwaysCheatBehaviour();
    @Test
    public void shouldAlwaysCheat(){
        Assert.assertEquals(MoveType.CHEAT.value,alwaysCheatBehaviour.makeMove());
    }
}
