import org.junit.Assert;
import org.junit.Test;

public class AlwaysCooperateBehaviourTest{
    AlwaysCooperateBehaviour alwaysCooperateBehaviour = new AlwaysCooperateBehaviour();
    @Test
    public void shouldAlwaysCooperate(){
        Assert.assertEquals(MoveType.COOPERATE.value,alwaysCooperateBehaviour.makeMove());
    }
}
