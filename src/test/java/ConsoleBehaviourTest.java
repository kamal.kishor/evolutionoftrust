import org.junit.Assert;
import org.junit.Test;

import java.util.Scanner;

public class ConsoleBehaviourTest {


    @Test
    public void getCooperateBehaviour(){
        Scanner sc=new Scanner("1");
        PlayerBehaviour playerBehaviour =new ConsoleBehaviour(sc);
        Assert.assertEquals(MoveType.COOPERATE.value, playerBehaviour.makeMove());
    }

    @Test
    public void getCheatBehaviour(){
        Scanner sc=new Scanner("0");
        PlayerBehaviour playerBehaviour =new ConsoleBehaviour(sc);
        Assert.assertEquals(MoveType.CHEAT.value, playerBehaviour.makeMove());
    }

}
