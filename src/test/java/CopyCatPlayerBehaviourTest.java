import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

public class CopyCatPlayerBehaviourTest {
    CopyCatPlayerBehaviour copyCatPlayer=new CopyCatPlayerBehaviour();

    @Test
    public void shouldReturnCooperateIfFirstMove(){
        Assert.assertEquals(MoveType.COOPERATE.value,copyCatPlayer.makeMove());
    }

    @Test
    public void shouldReturnCheatWhenPlayer1Cheat(){

        Map playerMoves=new HashMap();
        playerMoves.put("Player1Move",MoveType.CHEAT.value);
        playerMoves.put("Player2Move",MoveType.COOPERATE.value);
        copyCatPlayer.update(null, playerMoves);
        Assert.assertEquals(MoveType.CHEAT.value, copyCatPlayer.makeMove());
    }

}
