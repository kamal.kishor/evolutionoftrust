import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import sun.font.CCompositeGlyphMapper;

import java.io.Console;
import java.io.PrintStream;
import java.util.Scanner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class GameIntegrationTest {
    @Test
    public void play2ValidRound() {
        Player player1 = new Player(new ConsoleBehaviour(new Scanner("1\n1")));
        Player player2 = new Player(new ConsoleBehaviour(new Scanner("1\n0")));
        Machine machine = new Machine();
        PrintStream printStream = mock(PrintStream.class);

        Game game = new Game(player1, player2, machine, printStream);
        game.playRound(2);

        //Mockito.verify(printStream).println("P1Score: 1, P2Score: 5");
        Assert.assertEquals(1, player1.getScore());
        Assert.assertEquals(5, player2.getScore());
        verify(printStream).println("P1Score: 2, P2Score: 2");
        verify(printStream).println("P1Score: 1, P2Score: 5");

    }

    @Test
    public void shouldPlayBetweenCoolAndCheatPlayer() {
        Player player1 = new Player(new AlwaysCooperateBehaviour());
        Player player2 = new Player(new AlwaysCheatBehaviour());
        Machine machine = new Machine();
        PrintStream printStream = mock(PrintStream.class);

        Game game = new Game(player1, player2, machine, printStream);
        game.playRound(2);

        //Mockito.verify(printStream).println("P1Score: 1, P2Score: 5");
        Assert.assertEquals(-2, player1.getScore());
        Assert.assertEquals(6, player2.getScore());
        verify(printStream).println("P1Score: -1, P2Score: 3");
        verify(printStream).println("P1Score: -2, P2Score: 6");

    }

    @Test
    public void shouldPlayBetweenCoolAndConsolePlayer() {
        Player player1 = new Player(new AlwaysCooperateBehaviour());
        Player player2 = new Player(new ConsoleBehaviour(new Scanner("1\n0")));
        Machine machine = new Machine();
        PrintStream printStream = mock(PrintStream.class);

        Game game = new Game(player1, player2, machine, printStream);
        game.playRound(2);

        //Mockito.verify(printStream).println("P1Score: 1, P2Score: 5");
        Assert.assertEquals(1, player1.getScore());
        Assert.assertEquals(5, player2.getScore());
        verify(printStream).println("P1Score: 2, P2Score: 2");
        verify(printStream).println("P1Score: 1, P2Score: 5");

    }

    @Test
    public void shouldPlayBetweenCopyCatPlayerAndAlwaysCheatPlayer(){
        CopyCatPlayerBehaviour copyCatPlayerBehaviour = new CopyCatPlayerBehaviour();
        Player player1 = new Player(copyCatPlayerBehaviour);
        Player player2 = new Player(new AlwaysCheatBehaviour());
        Machine machine = new Machine();
        PrintStream printStream = mock(PrintStream.class);

        Game game = new Game(player1, player2, machine, printStream);
        game.addObserver(copyCatPlayerBehaviour);
        game.playRound(2);

        //Mockito.verify(printStream).println("P1Score: 1, P2Score: 5");
        Assert.assertEquals(-1, player1.getScore());
        Assert.assertEquals(3, player2.getScore());

    }


    @Test
    public void shouldPlayBetweenCopyCatPlayerAndAlwaysCooperatePlayer(){
        CopyCatPlayerBehaviour copyCatPlayerBehaviour = new CopyCatPlayerBehaviour();
        Player player1 = new Player(copyCatPlayerBehaviour);
        Player player2 = new Player(new AlwaysCooperateBehaviour());
        Machine machine = new Machine();
        PrintStream printStream = mock(PrintStream.class);

        Game game = new Game(player1, player2, machine, printStream);
        game.addObserver(copyCatPlayerBehaviour);
        game.playRound(2);

        //Mockito.verify(printStream).println("P1Score: 1, P2Score: 5");
        Assert.assertEquals(4, player1.getScore());
        Assert.assertEquals(4, player2.getScore());

    }

    @Test
    public void shouldPlayBetweenCopyCatPlayerAndCopyCatPlayer(){
        CopyCatPlayerBehaviour copyCatPlayerBehaviour = new CopyCatPlayerBehaviour();
        Player player1 = new Player(copyCatPlayerBehaviour);
        Player player2 = new Player(copyCatPlayerBehaviour);
        Machine machine = new Machine();
        PrintStream printStream = mock(PrintStream.class);

        Game game = new Game(player1, player2, machine, printStream);
        game.addObserver(copyCatPlayerBehaviour);
        game.playRound(2);

        //Mockito.verify(printStream).println("P1Score: 1, P2Score: 5");
        Assert.assertEquals(4, player1.getScore());
        Assert.assertEquals(4, player2.getScore());

    }



}
