import org.junit.Test;
import org.mockito.Mockito;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

public class GameTest {
    @Test
    public void playValidRound() {
        Player player1 = Mockito.mock(Player.class);
        Player player2 = Mockito.mock(Player.class);
        Machine machine = Mockito.mock(Machine.class);

        Mockito.when(player1.makeMove()).thenReturn(MoveType.COOPERATE.value);
        Mockito.when(player2.makeMove()).thenReturn(MoveType.CHEAT.value);

        Mockito.when(player1.getScore()).thenReturn(-1);
        Mockito.when(player2.getScore()).thenReturn(3);

        Map<String , Integer> result = new HashMap<>();
        result.put("P1Score", -1);
        result.put("P2Score", 3);
        Mockito.when(machine.getPlayerScores(MoveType.COOPERATE.value, MoveType.CHEAT.value)).thenReturn(result);

        PrintStream printStream = Mockito.mock(PrintStream.class);

        Game game = new Game(player1, player2, machine, printStream);
        game.playRound(1);

        Mockito.verify(printStream).println("P1Score: -1, P2Score: 3");

    }


}
