import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;

public class MachineTest {

    Machine machine = new Machine();

    @Test
    public void shouldReturnNullInCaseOfInvalidMove(){
        HashMap response=new HashMap();
        Assert.assertEquals(response,machine.getPlayerScores("xgfcfgh","yhfgjf"));

    }

    @Test
    public void shouldReturn2and2WhenP1CooperateAndP2Corporate(){
        HashMap response=new HashMap();
        response.put("P1Score",2);
        response.put("P2Score",2);
        Assert.assertEquals(response,machine.getPlayerScores(MoveType.COOPERATE.value,MoveType.COOPERATE.value));
    }

    @Test
    public void shouldReturn0and0WhenP1CheatAndP2Cheat(){
        HashMap response=new HashMap();
        response.put("P1Score",0);
        response.put("P2Score",0);
        Assert.assertEquals(response,machine.getPlayerScores(MoveType.CHEAT.value,MoveType.CHEAT.value));
    }

    @Test
    public void shouldReturnNegative1and3WhenP1CooperateAndP2Cheat(){
        HashMap response=new HashMap();
        response.put("P1Score",-1);
        response.put("P2Score",3);
        Assert.assertEquals(response,machine.getPlayerScores(MoveType.COOPERATE.value,MoveType.CHEAT.value));
    }

    @Test
    public void shouldReturn3andNegative1WhenP1CheatAndP2Cooperate(){
        HashMap response=new HashMap();
        response.put("P1Score",3);
        response.put("P2Score",-1);
        Assert.assertEquals(response,machine.getPlayerScores(MoveType.CHEAT.value,MoveType.COOPERATE.value));
    }


}
