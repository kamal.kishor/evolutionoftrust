import org.junit.Assert;
import org.junit.Test;
import java.util.Scanner;

public class PlayerTest {



    @Test
    public void makeCooperateMoveForOption_1() {



        Player player = new Player(new ConsoleBehaviour(new Scanner("0")));

        String moveType = player.makeMove();
        Assert.assertEquals(MoveType.CHEAT.value, moveType);
    }

    @Test
    public void makeCheatMoveForOption_2() {



        Player player = new Player(new ConsoleBehaviour(new Scanner("1")));

        String moveType = player.makeMove();
        Assert.assertEquals(MoveType.COOPERATE.value, moveType);
    }


}
